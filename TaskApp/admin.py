from django.contrib import admin

# Register your models here.
from .models import Task, Message
admin.site.register(Task)
admin.site.register(Message)
