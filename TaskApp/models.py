from django.db import models

# Create your models here.
from django.contrib.auth.models import User

class Task(models.Model):
    user = models.ForeignKey(User)
    task_name = models.CharField(max_length=20)
    task_desc = models.TextField(max_length=200)
    completed = models.BooleanField(default = False)
    date_created = models.DateTimeField(auto_now = True)
    image = models.ImageField(upload_to = 'Images/', default = 'Images/None/No-img.jpg', blank=True)
    doc = models.FileField(upload_to = 'Doc/', default = 'Doc/None/No-doc.pdf')

    def __str__(self):
        return "%s" % self.task_name

class Message(models.Model):
    sender = models.ForeignKey(User, related_name='sender', related_query_name='user')
    recipient = models.ForeignKey(User, related_name='recipient')
    message_text = models.TextField()
    date_created = models.DateTimeField(auto_now = True)

    def __str__(self):
        return "%s" % self.message_text

    def __str__(self):
        return "%s" % self.message_text
