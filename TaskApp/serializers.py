from rest_framework import serializers
from .models import Task, Message
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.shortcuts import redirect

class TaskSerializer(serializers.ModelSerializer):

    user = serializers.CharField(source = 'user.username', read_only = True)
    image = serializers.ImageField(max_length = None, use_url = True)
    doc = serializers.FileField(max_length = None, use_url = True)

    class Meta:
        model = Task
        fields = ('id', 'user', 'task_name', 'task_desc', 'completed', 'date_created', 'image', 'doc')

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'url', 'username', 'email', 'groups')

class MessageSerializer(serializers.ModelSerializer):

    # sender = serializers.CharField(source = 'user.username', read_only = True)
    # recipient = serializers.CharField(source = 'user.username', read_only = True)

    class Meta:
        model = Message
        fields = ('id', 'sender', 'recipient', 'message_text', 'date_created')

class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only = True)
    email = serializers.EmailField()

    next = '/api-auth/login/'

    def create(self,validated_data):
        user = get_user_model().objects.create(
            username = validated_data['username'],
            email = validated_data['email']
        )

        user.set_password(validated_data['password'])
        user.save()

        return user
    class Meta:
        model = get_user_model()
        fields = ('username', 'email', 'password')
