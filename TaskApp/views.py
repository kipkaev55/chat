from django.shortcuts import render

# Create your views here.
from django.contrib.auth import get_user_model
from rest_framework import filters, viewsets
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.generics import CreateAPIView
from .models import Task, Message
from .serializers import TaskSerializer, UserSerializer, MessageSerializer

from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from django.contrib.auth.models import User
from django.db.models import Q

class UserViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    http_method_names = ['get', 'head', 'options']
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

class TaskViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated,)
    # queryset = Task.objects.all()
    model = Task
    serializer_class = TaskSerializer
    filter_backends = (filters.DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter,)
    filter_fields = ('completed',)
    ordering = ('-date_created',)
    search_fields = ('task_desc',)

    def get_queryset(self):
        queryset = self.model.objects.all().filter(user = self.request.user,)
        return queryset

    def perform_create(self, serializer):
        return serializer.save(user = self.request.user)

class MessageViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated,)
    model = Message
    serializer_class = MessageSerializer
    filter_backends = (filters.DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter,)
    filter_fields = ('sender',)
    ordering = ('date_created',)
    search_fields = ('message_text',)

    def get_queryset(self):
        queryset = self.model.objects.all().filter(Q(sender = self.request.user) | Q(recipient = self.request.user))
        return queryset

    def perform_create(self, serializer):
        return serializer.save(sender = self.request.user)

class CreateUserView(CreateAPIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'login.html'
    permission_classes = (AllowAny,)
    serializer_class = UserSerializer

    def get(self, request):
        return Response()
    # def post(self, request):
    #     return Response()
class ChatView(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'chat.html'
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        return Response({'user_Id':self.request.user.id})
